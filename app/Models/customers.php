<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class customers extends Model
{
    use HasFactory;
    protected $fillable = [
    'CustomerId',
    'Firstname',
    'Lastname',
    'CustomerEmailAddress',
    'Phone',
    'StreetAddress',
    'Appartment_Suite_Unit',
    'City',
    'Province',
    'Country',
    'PostalCode',
    'Password',
    'Notes'
    ];
}
