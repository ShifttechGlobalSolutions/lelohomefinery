<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class orders extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */

    protected $fillable = [
        'OrderId',
        'CustomerId',
        'Amount',
        'OrderDate',
        'RequiredDate',
        'OrderStatus',

    ];
}
