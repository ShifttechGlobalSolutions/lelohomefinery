<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class catalogue extends Model
{
    use HasFactory;
    protected $fillable = [
        'ProductId',
        'CategoryName',
        'CategoryCode',
        'CategoryStatus',
        'ProductCategory',
        'ProductName',
        'ProductDescription',
        'ProductCode',
        'ProductPrice',
        'ProductOldPrice',
        'ProductDiscount',
        'StockQuantity',
        'file_path',
        'ProductPromotionStatus',
    ];
}
