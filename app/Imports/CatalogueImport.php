<?php

namespace App\Imports;

use App\Models\Catalogue;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CatalogueImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Catalogue([
            'ProductId' => $row[0],
            'ProductCategory' => $row[1],
            'ProductName' => $row[2],
            'ProductDescription' => $row[3],
            'ProductCode' => $row[4],
            'ProductPrice' => $row[5],
            'ProductOldPrice' => $row[6],
            'ProductDiscount' => $row[7],
            'StockQuantity' => $row[8],
            'file_path' => $row[9],
            'ProductPromotionStatus' => $row[10],
        ]);
    }
}
