<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\categories;
use App\Models\subCategory;
use Excel;
use App\Imports\CategoriesImport;
use Illuminate\Http\Eloquent;
use Illuminate\Support\Facades\DB;


class CategoriesController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

   
    public function index()
    {
        $category = DB::table('categories')->leftJoin('sub_categories', 'categories.CategoryCode', '=', 'sub_categories.CategoryCode')->select('categories.*','sub_categories.*')->paginate(1);
        // $category = categories::all();
        return view('admin/categories',['categories'=>$category]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     */

    
    public function store(Request $request)
    {

        $category = new categories;
        $timestamp = now();
      //  $category->CategoryId=$request->$timestamp;
        $category->CategoryName=$request->categoryName;
        $category->CategoryCode=$timestamp;
        $category->CategoryStatus=$request->categoryStatus;
       //  dd($category);
        $category->save();

        $subcategory = new subCategory;
        $subcategory->CategoryCode=$timestamp;
        $subcategory->SubCategoryName=$request->subCategoryName;
        $subcategory->SubDescription=$request->subCategoryDescription;
        $subcategory->save();

      return  back() ->with('Success','Data Saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function show(categories $categories)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function edit(Categories $categories)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Categories $categories)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Categories::find($id);
        $data->delete();
        return redirect('/admin/categories');
    }

    public function import(Request $request){
        Excel::import(new CategoriesImport, $request->file);
        return back();
    }
}
