<?php

namespace App\Http\Controllers;

use App\Models\ProductSearch;
use Illuminate\Http\Request;

class ProductSearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductSearch  $productSearch
     * @return \Illuminate\Http\Response
     */
    public function show(ProductSearch $productSearch)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductSearch  $productSearch
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductSearch $productSearch)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductSearch  $productSearch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductSearch $productSearch)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductSearch  $productSearch
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductSearch $productSearch)
    {
        //
    }
}
