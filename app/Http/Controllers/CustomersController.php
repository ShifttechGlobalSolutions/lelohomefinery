<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CustomersController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $customer= customer::all();
        // return view('admin/catalogue',['customer'=>$customer]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // ensure the request has a file before we attempt anything else.
        
            // Store the record, using the new file hashname which will be it's new filename identity.
            $customer = new Customer([
                "Firstname"=>$request->get('Firstname'),
                "Lastname"=>$request->get('Lastname'),
                "CustomerEmailAddress"=>$request->get('CustomerEmailAddress'),
                "Phone"=>$request->get('Phone'),
                "StreetAddress"=>$request->get('StreetAddress'),
                "Appartment_Suite_Unit"=>$request->get('Appartment_Suite_Unit'),
                "City"=>$request->get('City'),
                "Province"=>$request->get('Province'),
                "Country" => $request->get('Country'),
                "PostalCode" => $request->get('PostalCode'),
                "Password" => $request->get('Password'),
                "Notes"=>$request->get('Notes'),
            ]);
           // dd($catalogue);
            $customer->save(); // Finally, save the record.
    
        return back()->with('success', 'New customer successfully added');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Catalogue  $catalogue
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::find($id);
        $customer->Firstname = $request->get('Firstname');
        $customer->Lastname = $request->get('Lastname');
        $customer->CustomerEmailAddress = $request->get('CustomerEmailAddress');
        $customer->Phone = $request->get('Phone');
        $customer->StreetAddress = $request->get('StreetAddress');
        $customer->Appartment_Suite_Unit = $request->get('Appartment_Suite_Unit');
        $customer->City = $request->get('City');
        $customer->Province = $request->get('Province');
        // $catalogue->file_path = $request-file->hashName();
        $customer->Country = $request->get('Country');
        $customer->PostalCode = $request->get('PostalCode');
        $customer->Password = $request->get('Password');
        $customer->Notes = $request->get('Notes');

        $customer->update();

        return back()->with('success', 'Student updated successfully');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $catalogue
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Customer::find($id);
        $data->delete();
        return back();
    }
}
