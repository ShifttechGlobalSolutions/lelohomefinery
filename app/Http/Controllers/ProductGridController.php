<?php

namespace App\Http\Controllers;

use App\Models\ProductGrid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\catalogue;
use App\Models\categories;
use App\Models\subcategory;

class ProductGridController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $categories = DB::select('select * from categories ');
        //dd($category);
        $prod = catalogue::all();
        //$product = DB::select("select * from products where category= '$category'  ");


        return view('/shop')
            ->with('products', $prod)
            ->with('category', $categories);

    }

    public function viewByCategory($category){
        $productCategory = DB::table('categories as c')
        ->join('sub_categories as sc', 'c.CategoryCode', '=', 'sc.CategoryCode')
        ->join('catalogues as cat', 'c.CategoryName','=','cat.ProductCategory')
        ->whereRaw('sc.SubCategoryName = ?', [$category])
        ->selectRaw('*')->get();
        dd($productCategory);
        return view('/shop', ['productcategory', $productCategory]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductGrid  $productGrid
     * @return \Illuminate\Http\Response
     */
    public function show(ProductGrid $productGrid)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductGrid  $productGrid
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductGrid $productGrid)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductGrid  $productGrid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductGrid $productGrid)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductGrid  $productGrid
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductGrid $productGrid)
    {
        //
    }
}
