<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Hash;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Models\orders;
use App\Models\order_details;
use Illuminate\Support\Facades\DB;

class CustomAuthController extends Controller
{
    public function index()
    {
        return view('auth/login');
    }  

    public function customLogin(Request $request)
    {
        $Revenue = DB::table('select sum(amount) as totalSales from orders');
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
   
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            
            return redirect()->intended('dashboard')
                        ->withSuccess('Signed in');
        }
  
        return redirect("dashboard", ['Revenue'=>$Revenue])->withSuccess('Login details are not valid');
    }

    public function registration()
    {
        return view('auth/registration');
    }
      

    public function customRegistration(Request $request)
    {  
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
        ]);
           
        $data = $request->all();
        $check = $this->create($data);
         
        return redirect("login")->withSuccess('You have signed-in');
    }


    public function create(array $data)
    {
      return User::create([
        'name' => $data['name'],
        'email' => $data['email'],
        'password' => Hash::make($data['password'])
      ]);
    }    
    

    public function dashboard()
    {
        if(Auth::check()){

            $currentMonth = date('m');
            $currentMonth = date('m');
           
            //TotalByMonth
            $Jan = date('01');$Feb = date('02');$Mar = date('03');$Apr = date('04');$May = date('05');$Jun = date('06');
            $Jul = date('07');$Aug = date('08');$Sep = date('09');$Oct = date('10');$Nov = date('11');$Dec = date('12');

            $totalSalesJan = DB::table('orders')->whereRaw('MONTH(orderDate) = ?',[$Jan])->selectRaw('sum(amount) as totalSalesJan')->get();
            $totalSalesFeb = DB::table('orders')->whereRaw('MONTH(orderDate) = ?',[$Feb])->selectRaw('sum(amount) as totalSalesFeb')->get();
            $totalSalesMar = DB::table('orders')->whereRaw('MONTH(orderDate) = ?',[$Mar])->selectRaw('sum(amount) as totalSalesMar')->get();
            $totalSalesApr = DB::table('orders')->whereRaw('MONTH(orderDate) = ?',[$Apr])->selectRaw('sum(amount) as totalSalesApr')->get();
            $totalSalesMay = DB::table('orders')->whereRaw('MONTH(orderDate) = ?',[$May])->selectRaw('sum(amount) as totalSalesMay')->get();
            $totalSalesJun = DB::table('orders')->whereRaw('MONTH(orderDate) = ?',[$Jun])->selectRaw('sum(amount) as totalSalesJun')->get();
            $totalSalesJul = DB::table('orders')->whereRaw('MONTH(orderDate) = ?',[$Jul])->selectRaw('sum(amount) as totalSalesJul')->get();
            $totalSalesAug = DB::table('orders')->whereRaw('MONTH(orderDate) = ?',[$Aug])->selectRaw('sum(amount) as totalSalesAug')->get();
            $totalSalesSep = DB::table('orders')->whereRaw('MONTH(orderDate) = ?',[$Sep])->selectRaw('sum(amount) as totalSalesSep')->get();
            $totalSalesOct = DB::table('orders')->whereRaw('MONTH(orderDate) = ?',[$Oct])->selectRaw('sum(amount) as totalSalesOct')->get();
            $totalSalesNov = DB::table('orders')->whereRaw('MONTH(orderDate) = ?',[$Nov])->selectRaw('sum(amount) as totalSalesNov')->get();
            $totalSalesDec = DB::table('orders')->whereRaw('MONTH(orderDate) = ?',[$Dec])->selectRaw('sum(amount) as totalSalesDec')->get();
            
            // dd($totalSalesOct);
            // dd($totalSalesNov);
            $totalOrdersJan = DB::table('orders')->whereRaw('MONTH(orderDate) = ?',[$Jan])->selectRaw('count(orderId) as totalOrders')->get();
            $totalOrdersFeb = DB::table('orders')->whereRaw('MONTH(orderDate) = ?',[$Feb])->selectRaw('count(orderId) as totalOrders')->get();
            $totalOrdersMar = DB::table('orders')->whereRaw('MONTH(orderDate) = ?',[$Mar])->selectRaw('count(orderId) as totalOrders')->get();
            $totalOrdersApr = DB::table('orders')->whereRaw('MONTH(orderDate) = ?',[$Apr])->selectRaw('count(orderId) as totalOrders')->get();
            $totalOrdersMay = DB::table('orders')->whereRaw('MONTH(orderDate) = ?',[$May])->selectRaw('count(orderId) as totalOrders')->get();
            $totalOrdersJun = DB::table('orders')->whereRaw('MONTH(orderDate) = ?',[$Jun])->selectRaw('count(orderId) as totalOrders')->get();
            $totalOrdersJul = DB::table('orders')->whereRaw('MONTH(orderDate) = ?',[$Jul])->selectRaw('count(orderId) as totalOrders')->get();
            $totalOrdersAug = DB::table('orders')->whereRaw('MONTH(orderDate) = ?',[$Aug])->selectRaw('count(orderId) as totalOrders')->get();
            $totalOrdersSep = DB::table('orders')->whereRaw('MONTH(orderDate) = ?',[$Sep])->selectRaw('count(orderId) as totalOrders')->get();
            $totalOrdersOct = DB::table('orders')->whereRaw('MONTH(orderDate) = ?',[$Oct])->selectRaw('count(orderId) as totalOrders')->get();
            $totalOrdersNov = DB::table('orders')->whereRaw('MONTH(orderDate) = ?',[$Nov])->selectRaw('count(orderId) as totalOrders')->get();
            $totalOrdersDec = DB::table('orders')->whereRaw('MONTH(orderDate) = ?',[$Dec])->selectRaw('count(orderId) as totalOrders')->get();

            $recentCustOrders = DB::table('customers')->join('orders', 'customers.customerId', '=', 'orders.customerId')->select('customers.firstname as Firstname','customers.lastname as Lastname', 'orders.orderDate as OrderDate', 'orders.orderStatus as OrderStatus', 'orders.amount as Amount')->get() ;
            $TotalSalesByCurrentMonth = DB::table('orders')->whereRaw('MONTH(orderDate) = ?',[$currentMonth])->selectRaw('sum(amount) as totalSales')->get();
            $TotalOrdersByCurrentMonth = DB::table('orders')->whereRaw('MONTH(orderDate) = ?',[$currentMonth])->selectRaw('count(orderId) as totalOrders')->get();
            $TotalOrders = DB::table('orders')->selectRaw('count(orderId) as totalOrders')->get();
            $Revenue = DB::table('orders')->selectRaw('sum(amount) as totalSales')->get();
            // dd($Revenue);
            return view('admin/dashboard',compact(
                'Revenue',
                'TotalOrders',
                'TotalSalesByCurrentMonth',
                'TotalOrdersByCurrentMonth',
                'totalSalesJan',
                'totalSalesFeb',
                'totalSalesMar',
                'totalSalesApr',
                'totalSalesMay',
                'totalSalesJun',
                'totalSalesJul',
                'totalSalesAug',
                'totalSalesSep',
                'totalSalesOct',
                'totalSalesNov',
                'totalSalesDec',
                'totalOrdersJan',
                'totalOrdersFeb',
                'totalOrdersMar',
                'totalOrdersApr',
                'totalOrdersMay',
                'totalOrdersJun',
                'totalOrdersJul',
                'totalOrdersAug',
                'totalOrdersSep',
                'totalOrdersOct',
                'totalOrdersNov',
                'totalOrdersDec',
                'recentCustOrders'
            ));
        }
  
        return redirect("login")->withSuccess('You are not allowed to access');
    }
    // whereMonth('created_at', date('m'))

    public function signOut() {
        Session::flush();
        Auth::logout();
  
        return Redirect('login');
    }
}
