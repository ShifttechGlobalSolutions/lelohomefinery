<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\orders;
use App\Models\order_details;
use Illuminate\Support\Facades\DB;

class OrdersController extends Controller
{
    public function getTotalOrders(Request $request){
        
        $Revenue = DB::table('select sum(amount) from orders');
        
        return view('dashboard',['Revenue', $Revenue]);
    }
}
