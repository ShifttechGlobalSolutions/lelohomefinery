<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\catalogue;
use Illuminate\Support\Str;
use Excel;
use App\Imports\CatalogueImport;
use Illuminate\Support\Facades\DB;

class CatalogueController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $catalogue = DB::table('catalogues')->selectRaw('*')->paginate(1);
        return view('admin/catalogue',['catalogue'=>$catalogue]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // ensure the request has a file before we attempt anything else.
        if ($request->hasFile('file')) {

            $request->validate([
                'image' => 'mimes:jpeg,bmp,png,jpg' // Only allow .jpg, .bmp and .png file types.
            ]);

            // Save the file locally in the storage/public/ folder under a new folder named /productImage
            $request->file->store('public/productImage');

            $timestamp = now();
            // Store the record, using the new file hashname which will be it's new filename identity.
            $catalogue = new Catalogue([

                "ProductId"=>$timestamp,
                "ProductCategory"=>$request->get('productCategory'),
                "ProductName"=>$request->get('productName'),
                "ProductDescription"=>$request->get('productDescription'),
                "ProductCode"=>$request->get('productCode'),
                "ProductPrice"=>$request->get('productPrice'),
                "ProductOldPrice"=>$request->get('productOldPrice'),
                "ProductDiscount"=>$request->get('productDiscount'),
                "StockQuantity"=>$request->get('stockQuantity'),
                "file_path" => $request->file->hashName(),
                "ProductPromotionStatus"=>$request->get('productPromotionStatus'),
            ]);
           // dd($catalogue);
            $catalogue->save(); // Finally, save the record.
        }
        return back()->with('success', 'Student updated successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Catalogue  $catalogue
     * @return \Illuminate\Http\Response
     */
    public function show(Catalogue $catalogue)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Catalogue  $catalogue
     * @return \Illuminate\Http\Response
     */
    public function edit(Catalogue $catalogue)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Catalogue  $catalogue
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $catalogue = Catalogue::find($id);
        $catalogue->ProductCategory = $request->get('productCategory');
        $catalogue->ProductName = $request->get('productName');
        $catalogue->ProductDescription = $request->get('productDescription');
        $catalogue->ProductCode = $request->get('productCode');
        $catalogue->ProductPrice = $request->get('productPrice');
        $catalogue->ProductOldPrice = $request->get('productOldPrice');
        $catalogue->ProductDiscount = $request->get('productDiscount');
        $catalogue->StockQuantity = $request->get('stockQuantity');
        // $catalogue->file_path = $request-file->hashName();
        $catalogue->ProductPromotionStatus = $request->get('productPromotionStatus');
        $catalogue->ProductID = $request->get('productid');

        $catalogue->update();

        return redirect('admin/catalogue')->with('success', 'Student updated successfully');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Catalogue  $catalogue
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Catalogue::find($id);
        $data->delete();
        return back();
    }

    public function import(Request $request){
        Excel::import(new CatalogueImport, $request->file);
        return back();
    }
}
