<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\catalogue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    public function cart(Request $request){
        $product = session()->get('product');
        $image = $request->get('productImage');
        // dd($image);
        $id = $request->get('productId');
        if($product == null){
            $product =  [
                $id => [
                        "ProductId" => $request->get('productId'),
                        "ProductName" => $request->get('productName'),
                        "file_path" => $image,
                        "ProductPrice" => $request->get('productPrice'),
                        "ProductDescription" => $request->get('productDescription'),
                        "Size" => $request->get('size'),
                        "Color" => $request->get('color'),
                        "Qty" => $request->get('qty')
                ]
            ];
            session()->put('product', $product);
            return back()->with($id);
        }
    //    dd($product);
    
    // if cart not empty then check if this product exist then increment quantity 
    if(isset($product[$id])){
        $product[$id]['Qty']++;
        session()->put('product', $product);
        foreach($product as $key => $value){
            return back()->with($id);
        }
    }
    
    // if item not exist in cart then add to cart with quantity = 1
    $product[$id] =  [
            "ProductId" => $request->get('productId'),
            "ProductName" => $request->get('productName'),
            "file_path" => $image,
            "ProductPrice" => $request->get('productPrice'),
            "ProductDescription" => $request->get('productDescription'),
            "Size" => $request->get('size'),
            "Color" => $request->get('color'),
            "Qty" => $request->get('qty')
        ];
        session()->put('product', $product);
        return back()->with($id);
    }

    public function viewCart(){
        return view('viewCart');
    }

    public function cartQty(Request $request){
        session()->put('product', $product);
        return collect($product)->count();
    }

    public function update(Request $request)
    {
        if($request->id and $request->quantity)
        {
            $cart = session()->get('cart');
            $cart[$request->id]["quantity"] = $request->quantity;
            session()->put('cart', $cart);
            session()->flash('success', 'Cart updated successfully');
        }
    }
    public function remove($id)
    {  
        // dd($id);
        if($id) {
            $product = session()->get('product');
            if(isset($product[$id])) {
                unset($product[$id]);
                session()->put('product', $product);
            }
            session()->flash('success', 'Product removed successfully');
            
        }
        
        return back();
    }
}
