<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WishlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('wishlist');
    }

    public function addToWishlist(Request $request){
        $wishlistProduct = session()->get('wishlistProduct');
        $id = $request->get('productId');
        if($wishlistProduct == null){
            $wishlistProduct =  [
                $id => [
                        "ProductId" => $request->get('productId'),
                        "ProductImage" => $request->get('productImage'),
                        "ProductName" => $request->get('productName'),
                        "ProductPrice" => $request->get('productPrice'),
                        "ProductDescription" => $request->get('productDescription'),
                        "Size" => $request->get('size'),
                        "Color" => $request->get('color'),
                        "Qty" => $request->get('qty')
                ]
            ];
            session()->put('wishlistProduct', $wishlistProduct);
            dd($wishlistProduct->ProductImage);
            return redirect('wishlist')->with($id);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
