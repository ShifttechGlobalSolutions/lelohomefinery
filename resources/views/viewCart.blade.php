@extends("layouts.master")
@section("content")

<main>


<!-- page title area start -->
<section class="page__title p-relative d-flex align-items-center" data-background="{{asset('public/img/page-title/page-title-1.jpg')}}">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="page__title-inner text-center">
                    <h1>Your Cart</h1>
                    <div class="page__title-breadcrumb">                                 
                        <nav aria-label="breadcrumb">
                            @php ($id =2)
                        <ol class="breadcrumb justify-content-center">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page"> Cart</li>
                        </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- page title area end -->

<!-- Cart Area Strat-->
<section class="cart-area pt-100 pb-100">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <form action="#">
                    <div class="table-content table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="product-thumbnail">Images</th>
                                    <th class="cart-product-name">Product</th>
                                    <th class="product-price">Unit Price</th>
                                    <th class="product-quantity">Quantity</th>
                                    <th class="product-subtotal">Total</th>
                                    <th class="product-remove">Remove</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php ($total = 0) 
                            @if(session('product'))
                            @foreach(session('product') as $id => $details)
                            @php ($total += $details['ProductPrice'] * $details['Qty'])
                                <tr>
                                    <td class="product-thumbnail"><a href="product-details.html"><img style="width:50px" src="{{ asset('public/storage/productImage/'.$details['file_path']) }}" alt=""></a></td>
                                    <td class="product-name"><a href="product-details.html">{{ $details['ProductName'] }}</a></td>
                                    <td class="product-price"><span class="amount">{{ $details['ProductPrice'] }}</span></td>
                                    <td class="product-quantity">
                                        <div class="cart-plus-minus"><input type="text" value="{{ $details['Qty'] }}" /></div>
                                    </td>
                                    <td class="product-subtotal"><span class="amount">R {{sprintf('%0.2f', round(($details['Qty']*$details['ProductPrice'])+0,0001,2))}}</span></td>
                                    <td class="product-remove"><a href="{{ url('removeFromCart/'.$details['ProductId']) }}"><i class="fa fa-times"></i></a></td>
                                </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="coupon-all">
                                <div class="coupon">
                                    <input id="coupon_code" class="input-text" name="coupon_code" value=""
                                        placeholder="Coupon code" type="text">
                                    <button class="os-btn os-btn-black" name="apply_coupon" type="submit">Apply
                                        coupon</button>
                                </div>
                                <div class="coupon2">
                                    <button class="os-btn os-btn-black" name="update_cart" type="submit">Update cart</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 ml-auto">
                            <div class="cart-page-total">
                                <h2>Cart totals</h2>
                                <ul class="mb-20">
                                    <li>Subtotal <span>{{$total}}</span></li>
                                    <li>Total <span>{{$total}}</span></li>
                                </ul>
                                <a class="os-btn" href="{{ url('checkout') }}">Proceed to checkout</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- Cart Area End-->
</main>
@endsection