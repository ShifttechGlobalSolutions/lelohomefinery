
@extends("layouts.master")
@section("content")

<main>
        
        <!-- page title area start -->
        <section class="page__title p-relative d-flex align-items-center" data-background="{{asset('public/img/page-title/page-title-1.jpg')}}">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="page__title-inner text-center">
                            <h1>Wishlist</h1>
                            <div class="page__title-breadcrumb">                                 
                                <nav aria-label="breadcrumb">
                                <ol class="breadcrumb justify-content-center">
                                    <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page"> Wishlist</li>
                                </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- page title area end -->

        <!-- Cart Area Strat-->
        <section class="cart-area pt-100 pb-100">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <form action="#">
                            <div class="table-content table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="product-thumbnail">Images</th>
                                            <th class="cart-product-name">Product</th>
                                            <th class="product-price">Unit Price</th>
                                            <th class="product-subtotal">Action</th>
                                            <th class="product-remove">Remove</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @php ($total = 0) 
                                    @if(session('wishlistProduct'))
                                    @foreach(session('wishlistProduct') as $id => $details)
                                    @php ($total += $details['ProductPrice'] * $details['Qty'])
                                        <tr>
                                            <td class="product-thumbnail"><a href="product-details.html"><img src="{{ asset('public/storage/productImage/'.$details['ProductImage']) }}" alt=""></a></td>
                                            <td class="product-name"><a href="product-details.html">{{ $details['ProductName']}}</a></td>

                                            <td class="product-price"><span class="amount">{{ $details['ProductPrice'] }}</span></td>
                                            <td class="product-quantity">
                                                <button class="os-btn os-btn-black" type="submit">Add TO Cart</button>
                                            </td>
                                            <td class="product-remove"><a href="#"><i class="fa fa-times"></i></a></td>
                                        </tr>
                                    @endforeach
                                    @endif    
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- Cart Area End-->
    </main>
    @endsection