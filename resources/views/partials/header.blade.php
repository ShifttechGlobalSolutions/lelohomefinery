<header>
    <div id="header-sticky" class="header__area grey-bg">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4">
                    <div class="logo" style="display: flex; justify-content: space-even">
                        <a href="{{url('/')}}" style="line-height: 50px"><img src="{{asset('public/img/logo/lelo_logo.png')}}" style="width:50px" alt="logo"></a>
                        &nbsp;<div><h3>LELO'S</h3>
                        <h6 style="margin-top:-10px">HOME FINERY</h6></div>
                    </div>
                </div>
                <div class="col-xl-9 col-lg-9 col-md-8 col-sm-8">
                    <div class="header__right p-relative d-flex justify-content-between align-items-center">
                        <div class="main-menu d-none d-lg-block">
                            <nav class="header__action">
                                <ul>
                                    <li><a href="{{url('/shop')}}">Shop</a></li>
{{--                                    <li> <a href="javascript:void(0);">Categories</a>--}}
{{--                                        <ul class="extra-info">--}}
{{--                                            <li>--}}
{{--                                                <div class="my-account">--}}
{{--                                                    <div class="extra-title">--}}
{{--                                                        <h5>My Account</h5>--}}
{{--                                                    </div>--}}
{{--                                                    <ul>--}}
{{--                                                        <li><a href="login">My Account</a></li>--}}
{{--                                                        <li><a href="{{ url('wishlist') }}">Wishlist</a></li>--}}
{{--                                                        <li><a href="cart.html">Cart</a></li>--}}
{{--                                                        <li><a href="{{ url('checkout') }}">Checkout</a></li>--}}
{{--                                                        <li><a href="register.html">Create Account</a></li>--}}
{{--                                                    </ul>--}}
{{--                                                </div>--}}
{{--                                            </li>--}}
{{--                                            <li>--}}
{{--                                                <div class="lang">--}}
{{--                                                    <div class="extra-title">--}}
{{--                                                        <h5>Language</h5>--}}
{{--                                                    </div>--}}
{{--                                                    <ul>--}}
{{--                                                        <li><a href="#">English</a></li>--}}

{{--                                                    </ul>--}}
{{--                                                </div>--}}
{{--                                            </li>--}}

{{--                                        </ul>--}}
{{--                                    </li>--}}
                                    <li><a href="">AboutUs</a></li>


                                    <li><a href="">Contact</a></li>
                                </ul>
                            </nav>
                        </div>


                        <div class="mobile-menu-btn d-lg-none">
                            <a href="javascript:void(0);" class="mobile-menu-toggle"><i class="fas fa-bars"></i></a>
                        </div>
                        <div class="header__action">
                            <ul>
{{--                                <li><a href="#" class="search-toggle"><i class="ion-ios-search-strong"></i> Search</a></li>--}}
                                @if(session('product'))
                                @foreach(session('product') as $id => $details)
                                @endforeach
                                @endif
                                <li><a href="javascript:void(0);" class="cart"><i class="ion-bag"></i> Cart <span>(01)</span></a>
                                    <div class="mini-cart">
                                        <div class="mini-cart-inner">
                                        <ul class="mini-cart-list">
                                            @php ($total = 0)
                                            @if(session('product'))
                                            @foreach(session('product') as $id => $details)
                                            @php ($total += $details['ProductPrice'] * $details['Qty'])

                                                <li>
                                                    <div class="cart-img f-left">
                                                        <a href="product-details.html">
                                                            <img src="{{ asset('public/storage/productImage/'.$details['file_path']) }}" alt="" />

                                                        </a>
                                                    </div>
                                                    <div class="cart-content f-left text-left">
                                                        <h5>
                                                            <a href="product-details.html">{{ $details['ProductName'] }}</a>
                                                        </h5>
                                                        <div class="cart-price">
                                                            <span class="ammount">{{ $details['Qty'] }}&nbsp;&nbsp;&nbsp;<i class="fal fa-times"></i></span>&nbsp;&nbsp;&nbsp;
                                                            <span class="price">R {{sprintf('%0.2f', round( $details['ProductPrice']+0,0001,2)) }}</span>
                                                            <hr>
                                                            <h6 class="price">R {{sprintf('%0.2f', round(($details['Qty']*$details['ProductPrice'])+0,0001,2))}}</h6>
                                                        </div>
                                                    </div>

                                                    <div class="del-icon f-right mt-30">
                                                        <a href="{{ url('removeFromCart/'.$details['ProductId']) }}">
                                                            <i class="fal fa-times"></i>
                                                        </a>
                                                    </div>
                                                </li>
                                                <hr>
                                            @endforeach
                                            @endif
                                            </ul>
                                            <div class="total-price d-flex justify-content-between mb-30">
                                                <span>Subtotal:</span>
                                                <span>R {{sprintf('%0.2f', round($total+0,0001,2))}}</span>
                                            </div>
                                            <div class="checkout-link">
                                                <a href="{{ url('viewCart') }}" class="os-btn">view Cart</a>
                                                <a class="os-btn os-btn-black" href="{{ url('checkout') }}">Checkout</a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li> <a href="javascript:void(0);"><i class="far fa-bars"></i></a>
                                    <ul class="extra-info">
                                        <li>
                                            <div class="my-account">
                                                <div class="extra-title">
                                                    <h5>My Account</h5>
                                                </div>
                                                <ul>
                                                    <li><a href="login">My Account</a></li>
                                                    <li><a href="{{ url('wishlist') }}">Wishlist</a></li>
                                                    <li><a href="cart.html">Cart</a></li>
                                                    <li><a href="{{ url('checkout') }}">Checkout</a></li>
                                                    <li><a href="{{ url('register')}}">Create Account</a></li>
                                                </ul>
                                            </div>
                                        </li>


                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</header>
<!-- header area end -->

<!-- scroll up area start -->
<div class="scroll-up" id="scroll" style="display: none;">
    <a href="javascript:void(0);"><i class="fas fa-level-up-alt"></i></a>
</div>
<!-- scroll up area end -->

<!-- search area start -->
<section class="header__search white-bg transition-3">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="header__search-inner text-center">
                    <form action="#">
                        <div class="header__search-btn">
                            <a href="javascript:void(0);" class="header__search-btn-close"><i class="fal fa-times"></i></a>
                        </div>
                        <div class="header__search-header">
                            <h3>Search</h3>
                        </div>
                        <div class="header__search-categories">
                            <ul class="search-category">
                                <li><a href="shop.html">All Categories</a></li>
                                <li><a href="shop.html">Accessories</a></li>
                                <li><a href="shop.html">Chair</a></li>
                                <li><a href="shop.html">Tablet</a></li>
                                <li><a href="shop.html">Men</a></li>
                                <li><a href="shop.html">Women</a></li>

                            </ul>
                        </div>
                        <div class="header__search-input p-relative">
                            <input type="text" placeholder="Search for products... ">
                            <button type="submit"><i class="far fa-search"></i></button>
                        </div>
                    </form>


                </div>
            </div>
        </div>

    </div>
</section>
<div class="body-overlay transition-3"></div>
<!-- search area end -->

<!-- extra info area start -->
<section class="extra__info transition-3">
    <div class="extra__info-inner">
        <div class="extra__info-close text-right">
            <a href="javascript:void(0);" class="extra__info-close-btn"><i class="fal fa-times"></i></a>
        </div>
        <!-- side-mobile-menu start -->
        <nav class="side-mobile-menu d-block d-lg-none">
            <ul id="mobile-menu-active">
                <li class="active has-dropdown"><a href="index.html">Home</a>
                </li>
                <li class="mega-menu has-dropdown"><a href="shop.html">Shop</a>
                </li>
                <li class="has-dropdown"><a href="">AboutUs</a>

                </li>

                <li><a href="contact.html">Contact</a></li>
            </ul>
        </nav>
        <!-- side-mobile-menu end -->
    </div>
</section>
<div class="body-overlay transition-3"></div>
