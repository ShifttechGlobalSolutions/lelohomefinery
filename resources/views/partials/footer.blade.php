<section class="footer__area grey-bg p-relative">
    <div class="footer__top pt-75 pb-60">
        <div class="container">
            <div class="row">

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                    <div class="footer__widget mb-30">
                        <div class="footer__widget-title footer__widget-title-2 mb-25">
                            <h5>information</h5>
                        </div>
                        <div class="footer__widget-content">
                            <div class="footer__links footer__links-2">
                                <ul>

                                    <li><a href="#">Shipping Details</a></li>

                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">Terms & Condition</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-3 col-sm-6 col-12">
                    <div class="footer__widget mb-30">
                        <div class="footer__widget-title footer__widget-title-2 mb-25">
                            <h5>My Account</h5>
                        </div>
                        <div class="footer__widget-content">
                            <div class="footer__links footer__links-2">
                                <ul>

                                    <li><a href="{{url('checkout')}}">Checkout</a></li>
                                    <li><a href="{{url('cart')}}">Shopping Cart</a></li>
                                    <li><a href="{{url('wishlist')}}">Wishlist</a></li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                    <div class="footer__widget mb-30">
                        <div class="footer__widget-title footer__widget-title-2 mb-25">
                            <h5>Quick Links</h5>
                        </div>
                        <div class="footer__widget-content">
                            <div class="footer__links footer__links-2">
                                <ul>
                                    <li><a href="{{url('aboutUs')}}">About Us</a></li>
                                    <li><a href="{{url('shop')}}">Shop</a></li>
                                    <li><a href="{{url('contact')}}">Contact</a></li>

                                  
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
    <div class="footer__bottom footer__bottom-2">
        <div class="container">
            <div class="footer__bottom-inner footer__bottom-inner-2">
                <div class="row">
                    <div class="col-xl-8 col-lg-7">
                        <div class="footer__copyright footer__copyright-2">
                            <p>Copyright ©<script>document.write(new Date().getFullYear());</script> Lelo's Home Finery</a> all rights reserved. Powered by <a href="https://www.shifttechgs.com/">Shifttech Global Solutions</a></p>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-5">
                        <div class="footer__social footer__social-2 f-right">
                            <ul>
                                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
