@extends("layouts.master")
@section("content")

<main>

            <!-- page title area start -->
<<<<<<< HEAD
            <section class="page__title p-relative d-flex align-items-center" data-background="{{asset('public/img/page-title/page-title-1.jpg')}}">
                <div class="container">
=======
    <section class="page__title p-relative d-flex align-items-center" data-background="{{asset('public/img/page-title/page-title-1.jpg')}}">
                   <div class="container">
>>>>>>> 4715b5837066880388dbfd4fbc37a226baa370e8
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="page__title-inner text-center">
                                <h1>Register</h1>
                                <div class="page__title-breadcrumb">
                                    <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb justify-content-center">
                                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
<<<<<<< HEAD
                                        <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
=======
>>>>>>> 4715b5837066880388dbfd4fbc37a226baa370e8
                                        <li class="breadcrumb-item active" aria-current="page"> Register</li>
                                    </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- page title area end -->

            <!-- login Area Strat-->
            <section class="login-area pt-100 pb-100">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 offset-lg-2">
                            <div class="basic-login">
                                <h3 class="text-center mb-60">Signup From Here</h3>
                                <form action="{{ route('register.custom') }}" method="POST">
                                @csrf
                                    <label for="name">Username <span>**</span></label>
                                    <!-- <input id="name" type="text" placeholder="Enter Username" /> -->
                                    <input type="text" placeholder="Name" id="name" class="form-control" name="name"
                                    required autofocus>
                                    @if ($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif

                                    <label for="email-id">Email Address <span>**</span></label>
                                    <!-- <input id="email-id" type="text" placeholder="Email address..." /> -->
                                    <input type="text" placeholder="Email" id="email_address" class="form-control"
                                        name="email" required autofocus>
                                    @if ($errors->has('email'))
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                    @endif

                                    <label for="pass">Password <span>**</span></label>
                                    <!-- <input id="pass" type="password" placeholder="Enter password..." /> -->
                                    <input type="password" placeholder="Password" id="password" class="form-control"
                                    name="password" required>
                                    @if ($errors->has('password'))
                                    <span class="text-danger">{{ $errors->first('password') }}</span>
                                    @endif

                                    <div class="mt-10"></div>
                                    <button class="os-btn w-100">Register Now</button>
                                    <div class="or-divide"><span>or</span></div>
                                    <a href="{{ route('login') }}" class="os-btn os-btn-black w-100">login Now</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- login Area End-->
        </main>

        @endsection
