@extends("layouts.master")
@section("content")
<main>


    <!-- page title area start -->
    <section class="page__title p-relative d-flex align-items-center" data-background="{{asset('public/img/page-title/page-title-1.jpg')}}">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="page__title-inner text-center">
                        <h1>Your Cart</h1>
                        <div class="page__title-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb justify-content-center">
                                    <li class="breadcrumb-item"><a href="{{url('welcome')}}">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page"> Cart</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- page title area end -->

    <!-- Cart Area Strat-->
    <section class="cart-area pt-100 pb-100">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <form>
                        <div class="table-content table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
{{--                                    <th class="product-thumbnail">Images</th>--}}
                                    <th class="cart-product-name">Product</th>
                                    <th class="product-price">Unit Price</th>
                                    <th class="product-quantity">Quantity</th>
                                    <th class="product-subtotal">Total</th>
                                    <th class="product-update">Update</th>
                                    <th class="product-remove">Remove</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($cartstaf as $values)
                                <tr>
{{--                                    <td class="product-thumbnail"><a href="product-details.html"><img src="assets/img/shop/product/product-2.jpg" alt=""></a></td>--}}
                                    <td class="product-name"><a href="">{{$values['name']}}</a></td>
                                    <td class="product-price"><span class="amount">R{{sprintf('%0.2f', round($values['price']+0,0001,2))}}</span></td>
                                    <td class="product-quantity">
                                        <div class="cart-plus-minus update-cart">
                                            <input type="number" value="{{ $values['quantity'] }}" class="form-control quantity  " />
{{--                                            <input type="number" value="{{$values['quantity']}}" class="form-control quantity update-cart" />--}}
                                        </div>
                                    </td>
                                    <td class="product-subtotal"><span class="amount">R{{sprintf('%0.2f', round($values['price'] * $values['quantity']+0,0001,2))}}  </span></td>
                                    <td class=" update-cart" id="update-cart" ><a ><i class="fa fa-long-arrow-up"></i></a></td>

                                    <td class=" remove-from-cart"><a href="#"><i class="fa fa-times"></i></a></td>
                                </tr>

                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="coupon-all">

                                    <div class="coupon2">
                                        <button class="os-btn os-btn-black " id="update-cart"  type="submit">Update cart</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5 ml-auto">
                                <div class="cart-page-total">
                                    <h2>Cart totals</h2>
                                    <ul class="mb-20">
                                        @foreach($carttotal as $val)
                                        <li>Subtotal(Ex Vat) <span>R{{sprintf('%0.2f', round($val['subTotal']+0,0001,2))}}</span></li>
                                        <li>Total (Inc Vat-15%) <span>R{{sprintf('%0.2f', round($val['total']+0,0001,2))}}</span></li>
                                        @endforeach
                                    </ul>
                                    <a class="os-btn" href="checkout.html">Proceed to checkout</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- Cart Area End-->

</main>
@endsection


@section('scripts')

    <script type="text/javascript">



        $('#update-cart').change(function (e) {

            e.preventDefault();



            var ele = $(this);



            $.ajax({

                url: '{{ route('update.cart') }}',

                method: "patch",

                data: {

                    _token: '{{ csrf_token() }}',

                    id: ele.parents("tr").attr("data-id"),

                    quantity: ele.parents("tr").find(".quantity").val(),

                },

                success: function (response) {

                    window.location.reload();

                }

            });

        });



        $(".remove-from-cart").click(function (e) {

            e.preventDefault();



            var ele = $(this);



            if(confirm("Are you sure want to remove?")) {

                $.ajax({

                    url: '{{ route('remove.from.cart') }}',

                    method: "DELETE",

                    data: {

                        _token: '{{ csrf_token() }}',

                        id: ele.parents("tr").attr("data-id")

                    },

                    success: function (response) {

                        window.location.reload();

                    }

                });

            }

        });



    </script>

@endsection
