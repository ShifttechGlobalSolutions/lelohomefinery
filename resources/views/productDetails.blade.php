@extends("layouts.master")
@section("content")
<main>

    <!-- page title area start -->
    <section class="page__title p-relative d-flex align-items-center" data-background="assets/img/page-title/page-title-1.jpg">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="page__title-inner text-center">
                        <h1>Product Details</h1>
                        <div class="page__title-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb justify-content-center">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page"> Product details</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- page title area end -->


    <!-- shop details area start -->
    <section class="shop__area pb-65">
        <div class="shop__top grey-bg-6 pt-100 pb-90">
            <div class="container">
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{Session::get('success')}}
                    </div>
                @endif
                <div class="row">
                    @foreach($selected as $selected_products)
                    <div class="col-xl-6 col-lg-6">
                        <div class="product__modal-box d-flex">
                            <div class="product__modal-nav mr-20">
                                <nav>
                                    <div class="nav nav-tabs" id="product-details" role="tablist">
                                        <a class="nav-item nav-link active" id="pro-one-tab" data-toggle="tab" href="#pro-one" role="tab" aria-controls="pro-one" aria-selected="true">
                                            <div class="product__nav-img w-img">
                                                <img src="assets/img/shop/product/details/details-sm-1.jpg" alt="">
                                            </div>
                                        </a>
                                        <a class="nav-item nav-link" id="pro-two-tab" data-toggle="tab" href="#pro-two" role="tab" aria-controls="pro-two" aria-selected="false">
                                            <div class="product__nav-img w-img">
                                                <img src="assets/img/shop/product/quick-view/quick-sm-2.jpg" alt="">
                                            </div>
                                        </a>
                                        <a class="nav-item nav-link" id="pro-three-tab" data-toggle="tab" href="#pro-three" role="tab" aria-controls="pro-three" aria-selected="false">
                                            <div class="product__nav-img w-img">
                                                <img src="assets/img/shop/product/quick-view/quick-sm-3.jpg" alt="">
                                            </div>
                                        </a>
                                    </div>
                                </nav>
                            </div>
                            <div class="tab-content mb-20" id="product-detailsContent">
                                <div class="tab-pane fade show active" id="pro-one" role="tabpanel" aria-labelledby="pro-one-tab">
                                    <div class="product__modal-img product__thumb w-img">
                                        <img src="{{asset('public/img/shop/product/details/details-big-1.jpg')}}" alt="">
                                        <div class="product__sale ">
                                            <span class="new">new</span>
                                            <span class="percent">-16%</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6">
                        <div class="product__modal-content product__modal-content-2">
                            <h4><a href="product-details.html">{{ $selected_products->ProductName }}</a></h4>

                            <div class="product__price-2 mb-25">
                                <span>R{{sprintf('%0.2f', round($selected_products->ProductPrice+0,0001,2))}}</span>
                                <span class="old-price">R{{sprintf('%0.2f', round($selected_products->ProductOldPrice+0,0001,2))}}</span>
                            </div>
                            <div class="product__modal-des mb-30">
                                <p>{{ $selected_products->ProductDescription }}</p>
                            </div>
                            <div class="product__modal-form mb-30">
                                <form method="POST"  action="{{ route('cart.addToCart') }}">
                                    @csrf



                                    <div class="pro-quan-area d-sm-flex align-items-center">
                                        <div class="product-quantity-title">
                                            <label>Quantity</label>
                                        </div>
                                        <div class="product-quantity mr-20 mb-20">
                                            <div class="cart-plus-minus"> <input type="number" id="Quantity" name="Quantity" class="form-control" min="1" value="1">
                                                <input type="hidden" id="Product_id" name="Product_id" class="form-control" value="{{$selected_products->ProductId}}"></div>
                                        </div>
                                        <div class="pro-cart-btn form-group">
                                            <button  type="submit" class="add-cart-btn mb-20 ">+ Add to Cart</button>

                                        </div>
                                    </div>
                                </form>

                            </div>
                            <div class="product__tag mb-25">
                                <span>Category:</span>
                                <span><a href="#">{{ $selected_products->ProductCategory }}</a></span>

                            </div>

                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

    </section>
    <!-- shop details area end -->

    <!-- related products area start -->
    <section class="related__product pb-60">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="section__title-wrapper text-center mb-55">
                        <div class="section__title mb-10">
                            <h2>Trending Products</h2>
                        </div>
                        <div class="section__sub-title">
                            <p>Mirum est notare quam littera gothica quam nunc putamus parum claram!</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                    <div class="product__wrapper mb-60">
                        <div class="product__thumb">
                            <a href="product-details.html" class="w-img">
                                <img src="assets/img/shop/product/product-1.jpg" alt="product-img">
                                <img class="product__thumb-2" src="assets/img/shop/product/product-10.jpg" alt="product-img">
                            </a>
                            <div class="product__action transition-3">
                                <a href="#" data-toggle="tooltip" data-placement="top" title="Add to Wishlist">
                                    <i class="fal fa-heart"></i>
                                </a>
                                <a href="#" data-toggle="tooltip" data-placement="top" title="Compare">
                                    <i class="fal fa-sliders-h"></i>
                                </a>
                                <!-- Button trigger modal -->
                                <a href=""   data-toggle="modal" data-target="#productModalId">
                                    <i class="fal fa-search"></i>
                                </a>

                            </div>
                            <div class="product__sale">
                                <span class="new">new</span>
                                <span class="percent">-16%</span>
                            </div>
                        </div>
                        <div class="product__content p-relative">
                            <div class="product__content-inner">
                                <h4><a href="product-details.html">Wooden container Bowl</a></h4>
                                <div class="product__price transition-3">
                                    <span>$96.00</span>
                                    <span class="old-price">$96.00</span>
                                </div>
                            </div>
                            <div class="add-cart p-absolute transition-3">
                                <a href="#">+ Add to Cart</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                    <div class="product__wrapper mb-60">
                        <div class="product__thumb">
                            <a href="product-details.html" class="w-img">
                                <img src="assets/img/shop/product/product-2.jpg" alt="product-img">
                                <img class="product__thumb-2" src="assets/img/shop/product/product-8.jpg" alt="product-img">
                            </a>
                            <div class="product__action transition-3">
                                <a href="#" data-toggle="tooltip" data-placement="top" title="Add to Wishlist">
                                    <i class="fal fa-heart"></i>
                                </a>
                                <a href="#" data-toggle="tooltip" data-placement="top" title="Compare">
                                    <i class="fal fa-sliders-h"></i>
                                </a>
                                <!-- Button trigger modal -->
                                <a href="javascript:void(0);"   data-toggle="modal" data-target="#productModalId">
                                    <i class="fal fa-search"></i>
                                </a>

                            </div>
                            <div class="product__sale">
                                <span class="new">new</span>
                            </div>
                        </div>
                        <div class="product__content p-relative">
                            <div class="product__content-inner">
                                <h4><a href="product-details.html">Wooden container Bowl</a></h4>
                                <div class="product__price transition-3">
                                    <span>$35.00</span>
                                </div>
                            </div>
                            <div class="add-cart p-absolute transition-3">
                                <a href="#">+ Add to Cart</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                    <div class="product__wrapper mb-60">
                        <div class="product__thumb">
                            <a href="product-details.html" class="w-img">
                                <img src="assets/img/shop/product/product-3.jpg" alt="product-img">
                                <img class="product__thumb-2" src="assets/img/shop/product/product-7.jpg" alt="product-img">
                            </a>
                            <div class="product__action transition-3">
                                <a href="#" data-toggle="tooltip" data-placement="top" title="Add to Wishlist">
                                    <i class="fal fa-heart"></i>
                                </a>
                                <a href="#" data-toggle="tooltip" data-placement="top" title="Compare">
                                    <i class="fal fa-sliders-h"></i>
                                </a>
                                <!-- Button trigger modal -->
                                <a href="javascript:void(0);"   data-toggle="modal" data-target="#productModalId">
                                    <i class="fal fa-search"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product__content p-relative">
                            <div class="product__content-inner">
                                <h4><a href="product-details.html">Wooden container Bowl</a></h4>
                                <div class="product__price transition-3">
                                    <span>$40.00</span>
                                    <span class="old-price">$70.00</span>
                                </div>
                            </div>
                            <div class="add-cart p-absolute transition-3">
                                <a href="#">+ Add to Cart</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                    <div class="product__wrapper mb-60">
                        <div class="product__thumb">
                            <a href="product-details.html" class="w-img">
                                <img src="assets/img/shop/product/product-4.jpg" alt="product-img">
                                <img class="product__thumb-2" src="assets/img/shop/product/product-6.jpg" alt="product-img">
                            </a>
                            <div class="product__action transition-3">
                                <a href="#" data-toggle="tooltip" data-placement="top" title="Add to Wishlist">
                                    <i class="fal fa-heart"></i>
                                </a>
                                <a href="#" data-toggle="tooltip" data-placement="top" title="Compare">
                                    <i class="fal fa-sliders-h"></i>
                                </a>
                                <!-- Button trigger modal -->
                                <a href="javascript:void(0);"   data-toggle="modal" data-target="#productModalId">
                                    <i class="fal fa-search"></i>
                                </a>

                            </div>
                            <div class="product__sale">
                                <span class="new">new</span>
                            </div>
                        </div>
                        <div class="product__content p-relative">
                            <div class="product__content-inner">
                                <h4><a href="product-details.html">Wooden container Bowl</a></h4>
                                <div class="product__price transition-3">
                                    <span>$50.00</span>
                                </div>
                            </div>
                            <div class="add-cart p-absolute transition-3">
                                <a href="#">+ Add to Cart</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- related products area end -->

    <!-- shop modal start -->
    <!-- Modal -->
    <div class="modal fade" id="productModalId" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered product-modal" role="document">
            <div class="modal-content">
                <div class="product__modal-wrapper p-relative">
                    <div class="product__modal-close p-absolute">
                        <button   data-dismiss="modal"><i class="fal fa-times"></i></button>
                    </div>
                    <div class="product__modal-inner">
                        <div class="row">
                            <div class="col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12">
                                <div class="product__modal-box">
                                    <div class="tab-content mb-20" id="nav-tabContent">
                                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                            <div class="product__modal-img w-img">
                                                <img src="assets/img/shop/product/quick-view/quick-big-1.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                            <div class="product__modal-img w-img">
                                                <img src="assets/img/shop/product/quick-view/quick-big-2.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                                            <div class="product__modal-img w-img">
                                                <img src="assets/img/shop/product/quick-view/quick-big-3.jpg" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <nav>
                                        <div class="nav nav-tabs justify-content-between" id="nav-tab" role="tablist">
                                            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">
                                                <div class="product__nav-img w-img">
                                                    <img src="assets/img/shop/product/quick-view/quick-sm-1.jpg" alt="">
                                                </div>
                                            </a>
                                            <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">
                                                <div class="product__nav-img w-img">
                                                    <img src="assets/img/shop/product/quick-view/quick-sm-2.jpg" alt="">
                                                </div>
                                            </a>
                                            <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">
                                                <div class="product__nav-img w-img">
                                                    <img src="assets/img/shop/product/quick-view/quick-sm-3.jpg" alt="">
                                                </div>
                                            </a>
                                        </div>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-xl-7 col-lg-7 col-md-6 col-sm-12 col-12">
                                <div class="product__modal-content">
                                    <h4><a href="product-details.html">Wooden container Bowl</a></h4>
                                    <div class="rating rating-shop mb-15">
                                        <ul>
                                            <li><span><i class="fas fa-star"></i></span></li>
                                            <li><span><i class="fas fa-star"></i></span></li>
                                            <li><span><i class="fas fa-star"></i></span></li>
                                            <li><span><i class="fas fa-star"></i></span></li>
                                            <li><span><i class="fal fa-star"></i></span></li>
                                        </ul>
                                        <span class="rating-no ml-10">
                                                    3 rating(s)
                                                </span>
                                    </div>
                                    <div class="product__price-2 mb-25">
                                        <span>$96.00</span>
                                        <span class="old-price">$96.00</span>
                                    </div>
                                    <div class="product__modal-des mb-30">
                                        <p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram.</p>
                                    </div>
                                    <div class="product__modal-form">
                                        <form action="#">
                                            <div class="product__modal-input size mb-20">
                                                <label>Size <i class="fas fa-star-of-life"></i></label>
                                                <select>
                                                    <option>- Please select -</option>
                                                    <option> S</option>
                                                    <option> M</option>
                                                    <option> L</option>
                                                    <option> XL</option>
                                                    <option> XXL</option>
                                                </select>
                                            </div>
                                            <div class="product__modal-input color mb-20">
                                                <label>Color <i class="fas fa-star-of-life"></i></label>
                                                <select>
                                                    <option>- Please select -</option>
                                                    <option> Black</option>
                                                    <option> Yellow</option>
                                                    <option> Blue</option>
                                                    <option> White</option>
                                                    <option> Ocean Blue</option>
                                                </select>
                                            </div>
                                            <div class="product__modal-required mb-5">
                                                <span >Repuired Fiields *</span>
                                            </div>
                                            <div class="pro-quan-area d-lg-flex align-items-center">
                                                <div class="product-quantity-title">
                                                    <label>Quantity</label>
                                                </div>
                                                <div class="product-quantity">
                                                    <div class="cart-plus-minus"><input type="text" value="1" /></div>
                                                </div>
                                                <div class="pro-cart-btn ml-20">
                                                    <a href="#" class="add-cart-btn mr-10">+ Add to Cart</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<<<<<<< HEAD
</div>
<!-- shop modal end -->
</main>
@endsection
=======
    <!-- shop modal end -->


</main>
@endsection
>>>>>>> b8946b787a409aaaba3a58a6eaf75057f3e39910
