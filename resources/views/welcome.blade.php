@extends("layouts.master")
@section("content")
    <main>

        <div class="box-white grey-bg pt-50">
            <div class="container">
                <div class="box-white-inner">
                    <div class="row">
                        <div class="col-xl-12">

                            <!-- slider area start -->
                            <section class="slider__area slider__area-4 p-relative">
                                <div class="slider-active">
                                    <div class="single-slider single-slider-2 slider__height-4 d-flex align-items-center" data-background="{{asset('public/img/slider/slide-4.jpg')}}">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-xl-7 col-lg-7 col-md-9 col-sm-11 col-12">
                                                    <div class="slider__content slider__content-4">
                                                        <h2 data-animation="fadeInUp" data-delay=".2s">Kai-Leila Coffee Table </h2>
                                                       <a href="{{url('shop')}}" class="os-btn os-btn-2" data-animation="fadeInUp" data-delay=".6s">Discover now</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="single-slider single-slider-2 slider__height-4 d-flex align-items-center" data-background="{{asset('public/img/slider/slide-2.jpg')}}">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-xl-7 col-lg-7 col-md-9 col-sm-11 col-12">
                                                    <div class="slider__content slider__content-4">
                                                        <h2 data-animation="fadeInUp" data-delay=".2s">Facet Side Table</h2>
                                                           <a href="{{url('shop')}}" class="os-btn os-btn-2" data-animation="fadeInUp" data-delay=".6s">Discover now</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="single-slider single-slider-2 slider__height-4 d-flex align-items-center" data-background="{{asset('public/img/slider/slide-3.jpg')}}">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-xl-7 col-lg-7 col-md-9 col-sm-11 col-12">
                                                    <div class="slider__content slider__content-4">
                                                        <h2 data-animation="fadeInUp" data-delay=".2s">Emporium Woven Ottoman</h2>
                                                         <a href="{{url('shop')}}" class="os-btn os-btn-2" data-animation="fadeInUp" data-delay=".6s">Discover now</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <!-- slider area end -->

                            <!-- banner area start -->
                            <div class="banner__area pt-20">
                                <div class="container custom-container">
                                    <div class="row">
                                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                                            <div class="banner__item mb-30 p-relative">
                                                <div class="banner__thumb fix">
                                                    <a href="product-details.html" class="w-img"><img src="{{asset('public/img/shop/banner/banner-sm-3.jpg')}}" alt="banner"></a>
                                                </div>
                                                <div class="banner__content p-absolute transition-3">
                                                    <h5><a href="">Gymkhana Side Table </a></h5>
                                                    <a href="{{url('shop')}}" class="os-btn os-btn-2">Shop</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                                            <div class="banner__item mb-30 p-relative">
                                                <div class="banner__thumb fix">
                                                    <a href="product-details.html" class="w-img"><img src="{{asset('public/img/shop/banner/banner-sm-2.jpg')}}" alt="banner"></a>
                                                </div>
                                                <div class="banner__content p-absolute transition-3">
                                                    <h5><a href="">Shroom Ottoman</a></h5>
                                                    <a href="{{url('shop')}}" class="os-btn os-btn-2">Shop</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                                            <div class="banner__item mb-30 p-relative">
                                                <div class="banner__thumb fix">
                                                    <a href="product-details.html" class="w-img"><img src="{{asset('public/img/shop/banner/banner-sm-5.jpg')}}" alt="banner"></a>
                                                </div>
                                                <div class="banner__content p-absolute transition-3">
                                                    <h5><a href="">Tri-Max Side Table</a></h5>
                                                    <a href="{{url('shop')}}" class="os-btn os-btn-2">Shop</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- banner area end -->

                            <!-- product area start -->
                            <section class="product__area pt-60 pb-100">
                                <div class="container custom-container">
                                    <div class="row">
                                        <div class="col-xl-12">
                                            <div class="section__title-wrapper text-center mb-55">
                                                <div class="section__title mb-10">
                                                    <h2>Trending Products</h2>
                                                </div>
                                                <div class="section__sub-title">
                                                    <p>Mirum est notare quam littera gothica quam nunc putamus parum claram!</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-xl-12">
                                        <!-- @foreach($allProducts as $products)
                                        @php ($selectedId = $products->ProductId);
                                        @endforeach -->

                                            <div class="product__slider-4 owl-carousel">
                                                @foreach($allProducts as $products)
                                                @php ($selectedId = $products->ProductId)
                                                <div class="product__item">

                                                    <div class="product__wrapper mb-60">
                                                        <div class="product__thumb">
                                                            <a href="{{url('productDetails/'.$selectedId)}}" data-target="#productModalId" data-toggle="modal" class="w-img">
                                                                <img src="{{ asset('public/storage/productImage/'.$products->file_path) }}" alt="product-img">
                                                                <!-- <img src="{{asset('public/img/blog/blog-4.jpg')}}"> -->
                                                                <!-- <img src="{{asset('public/img/blog/blog-4.jpg')}}"> -->
                                                                <img name="productImage" class="product__thumb-2" src="{{ asset('public/storage/productImage/'.$products->file_path) }}" alt="product-img">
                                                            </a>
                                                            <div class="product__action transition-3">
                                                                <form class="needs-validation" method="POST"  action="wishlistProducts/{{$selectedId}}" enctype="multipart/form-data" novalidate>
                                                                @csrf
                                                                    <input type="hidden" name="productId" value="{{$products->ProductId}}">
                                                                    <input type="hidden" name="productName" value="{{$products->ProductName}}">
                                                                    <input type="hidden" name="productPrice" value="{{$products->ProductPrice}}">
                                                                    <input type="hidden" name="productDescription" value="{{$products->ProductDescription}}">
                                                                    <input type="hidden" name="size" value="{{$products->Size}}">
                                                                    <input type="hidden" name="color" value="{{$products->Color}}">
                                                                    <input type="hidden" name="qty" value="{{$products->Qty}}">
                                                                    <button type="submit" ><i class="fal fa-heart" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"></i></button>
                                                                </form>
                                                                <a href="#" data-toggle="tooltip" data-placement="top" title="Compare">
                                                                    <i class="fal fa-sliders-h"></i>
                                                                </a>
                                                                <!-- Button trigger modal -->
                                                                <a href="{{url('productDetails/'.$selectedId)}}" data-target="#productModalId" data-toggle="modal">
                                                                    <i class="fal fa-search"></i>
                                                                </a>

                                                            </div>
                                                        </div>
                                                        <div class="product__content p-relative">
                                                            <div class="product__content-inner">
                                                                <h4><a href="product-details.html">{{ $products->ProductName }}</a></h4>
                                                                <div class="product__price transition-3">
                                                                    <span>R{{sprintf('%0.2f', round($products->ProductPrice+0,0001,2))}}</span>
                                                                    <span class="old-price">R{{sprintf('%0.2f', round($products->ProductOldPrice+0,0001,2))}}</span>
                                                                </div>
                                                            </div>
                                                            <div class="add-cart p-absolute transition-3">
                                                                <a href="{{url('/'.$selectedId)}}" data-target="#productModalId" data-toggle="modal">+ Add to Cart</a>
                                                            </div>
                                                        </div>

                                                    </div>


                                                </div>
                                                @endforeach



                                            </div>

                                        </div>

                                    </div>

                                </div>
                            </section>
                            <!-- product area end -->

                            <!-- banner area start -->
                            <div class="banner__area-2 pb-60">
                                <div class="container-fluid p-0">
                                    <div class="row no-gutters">
                                        <div class="col-xl-6 col-lg-6">
                                            <div class="banner__item-2 banner-right p-relative mb-30 pr-15">
                                                <div class="banner__thumb fix">
                                                    <a href="product-details.html" class="w-img"><img src="{{asset('public/img/shop/banner/banner-sm-1.jpg')}}" alt="banner"></a>
                                                </div>
                                                <div class="banner__content-2 banner__content-4 p-absolute transition-3">
                                                    <span>Products Essentials</span>
                                                    <h4><a href="">Sisal Ottoman</a></h4>

                                                    <a href="{{url('shop')}}" class="os-btn os-btn-2">Shop</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-6">
                                            <div class=" banner__item-2 banner-left p-relative mb-30 pl-15">
                                                <div class="banner__thumb fix">
                                                    <a href="product-details.html" class="w-img"><img src="{{asset('public/img/shop/banner/banner-sm-3.jpg')}}" alt="banner"></a>
                                                </div>
                                                <div class="banner__content-2 banner__content-4 banner__content-4-right p-absolute transition-3">
                                                    <span>Products Furniture</span>
                                                    <h4><a href="product-details.html">Gymkhana Side Table</a></h4>

                                                    <a href="{{url('shop')}}" class="os-btn os-btn-2">Shop</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- banner area end -->

                            <!-- product offer area start -->
                            <section class="product__offer pb-45">
                                <div class="container custom-container">
                                    <div class="row">
                                        <div class="col-xl-6 col-lg-6 col-md-6">
                                            <div class="product__offer-inner mb-30">
                                                <div class="product__title mb-60">
                                                    <h4>New Arrivals</h4>
                                                </div>
                                                @foreach($newProducts as $values)
                                                <div class="product__offer-slider owl-carousel">
                                                    <div class="product__offer-wrapper">
                                                        <div class="sidebar__widget-content">
                                                            <div class="features__product-wrapper d-flex mb-20">
                                                                <div class="features__product-thumb mr-15">
                                                                    <a href="{{url('productDetails/'.$selectedId)}}" data-target="#productModalId" data-toggle="modal"><img style="width:80px" src="{{ asset('public/storage/productImage/'.$values->file_path) }}" alt="pro-sm-1"></a>
                                                                </div>
                                                                <div class="features__product-content">
                                                                    <h5><a href="product-details.html">{{ $values->ProductName }}</a></h5>
                                                                    <div class="price">
                                                                        <span>R{{sprintf('%0.2f', round($values->ProductPrice+0,0001,2))}}</span>
                                                                        <span class="price-old">R{{sprintf('%0.2f', round($values->ProductOldPrice+0,0001,2))}}</span>
                                                                        <div class="add-cart p-absolute transition-3">
                                                                            <a href="{{url('productDetails/'.$selectedId)}}" data-target="#productModalId" data-toggle="modal">+ Add to Cart</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                                @endforeach

                                            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-md-6">
                                            <div class="product__offer-inner mb-30">
                                                <div class="product__title mb-60">
                                                    <h4>On-Sale Products</h4>
                                                </div>
                                                @foreach($productsOnSale as $value)
                                                <div class="product__offer-slider owl-carousel">
                                                    <div class="product__offer-wrapper">
                                                        <div class="sidebar__widget-content">
                                                            <div class="features__product-wrapper d-flex mb-20">
                                                                <div class="features__product-thumb mr-15">
                                                                    <a href="{{url('productDetails/'.$selectedId)}}" data-target="#productModalId" data-toggle="modal"><img style="width:80px" src="{{ asset('public/storage/productImage/'.$value->file_path) }}" alt="pro-sm-1"></a>
                                                                </div>
                                                                <div class="features__product-content">
                                                                    <h5><a href="{{url('productDetails/'.$selectedId)}}" data-target="#productModalId" data-toggle="modal">{{ $value->ProductName }}</a></h5>
                                                                    <div class="price">
                                                                        <span>R{{sprintf('%0.2f', round($value->ProductPrice+0,0001,2))}}</span>
                                                                        <span class="price-old">R{{sprintf('%0.2f', round($value->ProductOldPrice+0,0001,2))}}</span>
                                                                        <div class="add-cart p-absolute transition-3">
                                                                            <a href="{{ url('productDetails/'.$products->ProductId)}}" data-target="#productModalId" data-toggle="modal">+ Add to Cart</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                                @endforeach

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </section>
                            <!-- product offer area end -->


                            <!-- subscribe area start -->
                            <section class="subscribe__area pb-100">
                                <div class="container custom-container">
                                    <div class="subscribe__inner pt-95">
                                        <div class="row">
                                            <div class="col-xl-8 offset-xl-2">
                                                <div class="subscribe__content text-center">
                                                    <h2>Get Discount Info</h2>
                                                    <p>Subscribe to the Outstock mailing list to receive updates on new arrivals, special offers and other discount information.</p>
                                                    <div class="subscribe__form">
                                                        <form action="#">
                                                            <input type="email" placeholder="Subscribe to our newsletter...">
                                                            <button class="os-btn os-btn-2 os-btn-3">subscribe</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <!-- subscribe area end -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- shop modal start -->
        <!-- Modal -->
        <div class="modal fade" id="productModalId" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" t>
            <div class="modal-dialog modal-dialog-centered product-modal" role="document">
                <div class="modal-content">
                    <div class="product__modal-wrapper p-relative">
                        <div class="product__modal-close p-absolute">
                            <button   data-dismiss="modal"><i class="fal fa-times"></i></button>
                        </div>
                        @foreach($returnedResult as $products)
                                @endforeach
                        <div class="product__modal-inner">
                            <div class="row">
                                <div class="col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12">
                                    <div class="product__modal-box">
                                        <div class="tab-content mb-20" id="nav-tabContent">
                                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                                <div class="product__modal-img w-img">
                                                    <img src="{{ asset('public/storage/productImage/'.$products->file_path) }}" alt="">
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                                <div class="product__modal-img w-img">
                                                    <img src="{{asset('public/img/shop/product/quick-view/quick-big-2.jpg')}}" alt="">
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                                                <div class="product__modal-img w-img">
                                                    <img src="{{asset('public/img/shop/product/quick-view/quick-big-3.jpg')}}" alt="">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-xl-7 col-lg-7 col-md-6 col-sm-12 col-12">
                                    <div class="product__modal-content">
                                        <h4><a href="product-details.html">{{ $products->ProductName }}</a></h4>
                                        <div class="rating rating-shop mb-15">
                                            <ul>
                                                <li><span><i class="fas fa-star"></i></span></li>
                                                <li><span><i class="fas fa-star"></i></span></li>
                                                <li><span><i class="fas fa-star"></i></span></li>
                                                <li><span><i class="fas fa-star"></i></span></li>
                                                <li><span><i class="fal fa-star"></i></span></li>
                                            </ul>
                                            <span class="rating-no ml-10">
                                                    4 rating(s)
                                                </span>
                                        </div>
                                        <div class="product__price-2 mb-25">
                                            <span>R {{sprintf('%0.2f', round($products->ProductPrice+0,0001,2)) }}</span>
                                            <span class="old-price">R {{sprintf('%0.2f', round($products->ProductOldPrice+0,0001,2)) }}</span>
                                        </div>
                                        <div class="product__modal-des mb-30">
                                            <p>{{ $products->ProductDescription }}</p>
                                        </div>
                                        <div class="product__modal-form">
                                            <form class="needs-validation" method="POST"  action="cart/{{$products->ProductId}}" enctype="multipart/form-data" novalidate>
                                            @csrf
                                            <input type="hidden" name="productId" value="{{$products->ProductId}}">
                                            <input type="hidden" name="productName" value="{{$products->ProductName}}">
                                            <input type="hidden" name="productImage" value="{{$products->file_path}}">
                                            <input type="hidden" name="productPrice" value="{{$products->ProductPrice}}">
                                            <input type="hidden" name="productDescription" value="{{$products->ProductDescription}}">
                                            <div class="product__modal-input size mb-20">
                                                    <label>Size <i class="fas fa-star-of-life"></i></label>
                                                    <select name="size">
                                                        <option>- Please select -</option>
                                                        <optgroup label="Blanket & Sheets">
                                                            <option> 3/4 150 x 200 cm</option>
                                                            <option> Double 200 x 200 cm</option>
                                                            <option> King 230 x 220 cm</option>
                                                            <option> Queen 230 x 200 cm</option>
                                                            <option> Super King 260 x 200 cm</option>
                                                        </optgroup>
                                                        <optgroup label="Floor Lamps">
                                                            <option> Short floor lamps</option>
                                                            <option> Average floor lamps</option>
                                                            <option> Tall floor lamps</option>
                                                        </optgroup>
                                                    </select>
                                                </div>
                                                <div class="product__modal-input color mb-20">
                                                    <label>Color <i class="fas fa-star-of-life"></i></label>
                                                    <select name="color">
                                                        <option>- Please select -</option>
                                                        <option> Black</option>
                                                        <option> Yellow</option>
                                                        <option> Blue</option>
                                                        <option> White</option>
                                                        <option> Ocean Blue</option>
                                                    </select>
                                                </div>
                                                <div class="product__modal-required mb-5">
                                                    <span >Repuired Fiields *</span>
                                                </div>
                                                <div class="pro-quan-area d-lg-flex align-items-center">
                                                    <div class="product-quantity-title">
                                                        <label>Quantity</label>
                                                    </div>
                                                    <div class="product-quantity">
                                                        <div class="cart-plus-minus"><input type="text" value="1" name="qty" /></div>
                                                    </div>
                                                    <div class="pro-cart-btn ml-20">
                                                        <button type="submit" class="btn btn-primary">Add to Cart</button>
                                                        <!-- <a href="{{url('cart/'.$products->ProductId)}}" class="os-btn os-btn-black os-btn-3 mr-10">+ Add to Cart</a> -->
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- shop modal end -->

    </main>
@endsection
