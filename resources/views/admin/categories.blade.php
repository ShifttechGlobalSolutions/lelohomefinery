@extends("layouts.admin_master")
@section("content")

<div class="page-wrapper">
        <div class="content container-fluid">

            <!-- Page Header -->
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <h3 class="page-title">Product Categories</h3>
                    </div>
                    <div class="col-auto text-right">
                        <a class="btn btn-white filter-btn" href="javascript:void(0);" id="filter_search">
                            <i class="fas fa-filter"></i>
                        </a>

                        <a href="" class="btn btn-primary "  data-target="#importsCategoryModal" data-toggle="modal">
                            {{--                            <i class="fas fa-plus">--}}
                            Import Categories</i>
                        </a>
                        <a href="" class="btn btn-primary "  data-target="#categoryModal" data-toggle="modal">
                            {{--                            <i class="fas fa-plus">--}}
                            Add Category</i>
                        </a>



                    </div>


                </div>
            </div>
            <!-- /Page Header -->

            <!-- Search Filter -->
            <div class="card filter-card" id="filter_inputs">
                <div class="card-body pb-0">
                    <form action="#" method="post">
                        <div class="row filter-row">
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label>Category</label>
                                    <select class="form-control select">
                                        <option>Select category</option>
                                        <option>Automobile</option>
                                        <option>Construction</option>
                                        <option>Interior</option>
                                        <option>Cleaning</option>
                                        <option>Electrical</option>
                                        <option>Carpentry</option>
                                        <option>Computer</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label>From Date</label>
                                    <div class="cal-icon">
                                        <input class="form-control datetimepicker" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label>To Date</label>
                                    <div class="cal-icon">
                                        <input class="form-control datetimepicker" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <button class="btn btn-primary btn-block" type="submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /Search Filter -->

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover table-center mb-0 datatable">
                                <thead>
                                    <tr>
                                        <th>#</th>


                                        <th>Category</th>
                                        <th>Category Status</th>
                                        <th>Sub Category</th>
                                        <th class="text-right">Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($categories as $category)
                                    <tr>
                                        <td>{{$category->id}}</td>
                                        <td>{{$category->CategoryName}}</td>
                                        <td>{{$category->CategoryStatus}}</td>
                                        <td>{{$category->SubCategoryName}}</td>

                                        <td class="text-right">
                                            <a href="{{'/categories/edit/'.$category->id}}" class= " btn btn-sm bg-success-light mr-2">	<i class="far fa-edit mr-1"></i> Edit</a>
                                            <a href="{{ url('categoryDelete/'.$category->id) }}" class= " btn btn-sm bg-danger-light mr-2">	<i class="fa fa-trash"></i> Delete</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <span>
                                    {!! $categories->links("pagination::bootstrap-4")!!}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="categoryModal" class="modal fade" data-backdrop="static" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title">Add Product Category</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                    <form class="needs-validation" method="POST"  action="{{ route('categories.store') }}">
                        @csrf

                        <h6>Category</h6>
                        <hr>
                        <div class="form-group row ">
                            <div class="col-sm-12">
                                <select name="categoryName" class="custom-select" id="categoryName" required>
                                    <option value=""> Category Name</option>
                                    <option value="Furniture">Furniture</option>
                                    <option value="Bathroom">Bathroom</option>
                                    <option value="Bedroom">Bedroom</option>
                                    <option value="Lighting">Lighting</option>
                                    <option value="Pendants">Pendants</option>
                                </select>
                                <!-- <input type="text" class="form-control" id="categoryName" name="categoryName" placeholder="Category Name" required> -->
                            </div>
                        </div>
                        <div class="form-group row">


                            <div class="col-sm-6">
                                <select class="custom-select" id="categoryStatus" name="categoryStatus" required>
                                    <option value=""> Category Status</option>
                                    <option value="Visible">Visible</option>
                                    <option value="Hidden">Hidden</option>
                                </select>
                            </div>

                        </div>

                        <hr>
                        <h6>Sub-Category</h6>

                        <hr>
                        <div class="form-group row ">
                            <div class="col-sm-12">
                                <select name="subCategoryName" class="custom-select" id="subCategoryName" required>
                                        <option value=""> Category Name</option>
                                        <optgroup label="Furniture">
                                            <option value="Stools & Chairs">Stools & Chairs</option>
                                            <option value="Ottoman">Ottoman</option>
                                            <option value="Coffee Tables">Coffee Tables</option>
                                            <option value="Side Tables">Side Tables</option>
                                        </optgroup>
                                        <optgroup label="Bathroom">
                                            <option value="Amenities">Amenities</option>
                                            <option value="Bath Robes">Bath Robes</option>
                                            <option value="Rugs">Rugs</option>

                                        </optgroup>
                                        <optgroup label="Bedroom">
                                            <option value="Duvet Covers">Duvet Covers</option>
                                            <option value="Fitted sheets">Fitted sheets</option>
                                            <option value="Flat sheets">Flat sheets</option>
                                            <option value="Pillowcases">Pillowcases</option>
                                            <option value="Blankets">Blankets</option>
                                            <option value="Scatter Cushions">Scatter Cushions</option>
                                            <option value="Throws">Throws</option>
                                        </optgroup>
                                        <optgroup label="Lighting">
                                            <option value="Desk Lamps">Desk Lamps</option>
                                            <option value="Birch Lampstand">Birch Lampstand</option>
                                            <option value="Cotton Floor Lanterns">Cotton Floor Lanterns</option>
                                            <option value="Jute & Woven Lampstand">Jute & Woven Lampstand</option>
                                            <option value="Metal Lampstand">Metal Lampstand</option>
                                            <option value="Parchment">Parchment</option>
                                            <option value="Wooden Contempo">Wooden Contempo</option>
                                        </optgroup>
                                        <optgroup label="Pendants">
                                            <option value="Birch">Birch</option>
                                            <option value="Black & Gold">Black & Gold</option>
                                            <option value="Bubble">Bubble</option>
                                            <option value="Fabric">Fabric</option>
                                            <option value="Felt Pendants">Felt Pendants</option>
                                            <option value="Jute & Sisal">Jute & Sisal</option>
                                            <option value="Metal">Metal</option>
                                            <option value="Parchment">Parchment</option>
                                            <option value="Pleated/Fabric">Pleated/Fabric</option>
                                            <option value="Pro Felt">Pro Felt</option>
                                            <option value="Raffia">Raffia</option>
                                            <option value="T-Yarn">T-Yarn</option>
                                            <option value="Textures">Textures</option>
                                            <option value="Wooden Contempo">Wooden Contempo</option>
                                            <option value="Wooden Resin/Rope">Wooden Resin/Rope</option>
                                        </optgroup>

                                    </select>
                                </div>
                            </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <textarea rows="3" type="text" class="form-control" id="subCategoryDescription" name="subCategoryDescription" placeholder="Sub-Category Description" required></textarea>
                            </div>
                        </div>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save Category</button>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <!-- Bulk Category Imports modal -->
    <div id="importsCategoryModal" class="modal fade" data-backdrop="static" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title">Import Categories from an Excel file</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                    <form class="needs-validation" method="POST" action="{{ route('import_Category') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">

                            <div class="col-sm-6">
                                <label for="file">Choose a file</label>
                            </div>

                            <div class="col-sm-6">
                                <input type="file" class="form-control" id="file" name="file" required>
                            </div>

                        </div>

                        <hr>

                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save Category</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
    @endsection
