@extends("layouts.admin_master")
@section("content")
<?php
foreach ($totalSalesJan as $TotalSalesJan) {}
foreach ($totalSalesFeb as $TotalSalesFeb) {}
foreach ($totalSalesMar as $TotalSalesMar) {}
foreach ($totalSalesApr as $TotalSalesApr) {}
foreach ($totalSalesMay as $TotalSalesMay) {}
foreach ($totalSalesJun as $TotalSalesJun) {}
foreach ($totalSalesJul as $TotalSalesJul) {}
foreach ($totalSalesAug as $TotalSalesAug) {}
foreach ($totalSalesSep as $TotalSalesSep) {}
foreach ($totalSalesOct as $TotalSalesOct) {}
foreach ($totalSalesNov as $TotalSalesNov) {}
foreach ($totalSalesDec as $TotalSalesDec) {}

$dataPoints = array(
	array("label" => "Jan", "y" => ($TotalSalesJan->totalSalesJan != null) ? $TotalSalesJan->totalSalesJan : 0),
	array("label" => "Feb", "y" => ($TotalSalesFeb->totalSalesFeb != null) ? $TotalSalesFeb->totalSalesFeb : 0),
	array("label" => "Mar" , "y" => ($TotalSalesMar->totalSalesMar != null) ? $TotalSalesMar->totalSalesMar : 0),
	array("label" => "Apr" , "y" => ($TotalSalesApr->totalSalesApr != null) ? $TotalSalesApr->totalSalesApr : 0),
	array("label" => "May" , "y" => ($TotalSalesMay->totalSalesMay != null) ? $TotalSalesMay->totalSalesMay : 0),
	array("label" => "Jun" , "y" => ($TotalSalesJun->totalSalesJun != null) ? $TotalSalesJun->totalSalesJun : 0),
	array("label" => "Jul" , "y" => ($TotalSalesJul->totalSalesJul != null) ? $TotalSalesJul->totalSalesJul : 0),
	array("label" => "Aug" , "y" => ($TotalSalesAug->totalSalesAug != null) ? $TotalSalesAug->totalSalesAug : 0),
	array("label" => "Sep" , "y" => ($TotalSalesSep->totalSalesSep != null) ? $TotalSalesSep->totalSalesSep : 0),
	array("label" => "Oct" , "y" => ($TotalSalesOct->totalSalesOct != null) ? $TotalSalesOct->totalSalesOct : 0),
	array("label" => "Nov" , "y" => ($TotalSalesNov->totalSalesNov != null) ? $TotalSalesNov->totalSalesNov : 0),
	array("label" => "Dec" , "y" => ($TotalSalesDec->totalSalesDec != null) ? $TotalSalesDec->totalSalesDec : 0),
 );

foreach ($totalOrdersJan as $TotalOrdersJan) {}
foreach ($totalOrdersFeb as $TotalOrdersFeb) {}
foreach ($totalOrdersMar as $TotalOrdersMar) {}
foreach ($totalOrdersApr as $TotalOrdersApr) {}
foreach ($totalOrdersMay as $TotalOrdersMay) {}
foreach ($totalOrdersJun as $TotalOrdersJun) {}
foreach ($totalOrdersJul as $TotalOrdersJul) {}
foreach ($totalOrdersAug as $TotalOrdersAug) {}
foreach ($totalOrdersSep as $TotalOrdersSep) {}
foreach ($totalOrdersOct as $TotalOrdersOct) {}
foreach ($totalOrdersNov as $TotalOrdersNov) {}
foreach ($totalOrdersDec as $TotalOrdersDec) {}

 $dataPoints1 = array(
	array("label"=>"Jan", "symbol" => "Jan","y"=>$TotalOrdersJan->totalOrders),
	array("label"=>"Feb", "symbol" => "Feb","y"=>$TotalOrdersFeb->totalOrders),
	array("label"=>"Mar", "symbol" => "Mar","y"=>$TotalOrdersMar->totalOrders),
	array("label"=>"Apr", "symbol" => "Apr","y"=>$TotalOrdersApr->totalOrders),
	array("label"=>"May", "symbol" => "May","y"=>$TotalOrdersMay->totalOrders),
	array("label"=>"Jun", "symbol" => "Jun","y"=>$TotalOrdersJun->totalOrders),
	array("label"=>"Jul", "symbol" => "Jul","y"=>$TotalOrdersJul->totalOrders),
	array("label"=>"Aug", "symbol" => "Aug","y"=>$TotalOrdersAug->totalOrders),
    array("label"=>"Sep", "symbol" => "Sep","y"=>$TotalOrdersSep->totalOrders),
	array("label"=>"Oct", "symbol" => "Oct","y"=>$TotalOrdersOct->totalOrders),
	array("label"=>"Nov", "symbol" => "Nov","y"=>$TotalOrdersNov->totalOrders),
	array("label"=>"Dec", "symbol" => "Dec","y"=>$TotalOrdersDec->totalOrders),

)

 ?>
 <script>
window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	theme: "light5",
	title:{
		text: ""
	},
	// axisX: {
	// 	valueFormatString: "MMM"
	// },
	axisY: {
		title: "Amount (R)",
		includeZero: true,
		maximum: 1500
	},
	data: [{
		type: "splineArea",
		color: "#6599FF",
		// xValueType: "dateTime",
		// xValueFormatString: "DD MMM",
		yValueFormatString: "R###,###",
		dataPoints: <?php echo json_encode($dataPoints); ?>
	}]
});

chart.render();

 var chart1 = new CanvasJS.Chart("chartContainer1", {
     theme: "light5",
     animationEnabled: true,
     title: {
         text: " "
     },
     data: [{
         type: "doughnut",
         indexLabel: "{symbol} - {y}",
         yValueFormatString: "### ###",
        //  showInLegend: true,
        //  legendText: "{label} : {y}",
         dataPoints: <?php echo json_encode($dataPoints1, JSON_NUMERIC_CHECK); ?>
     }]
 });
 chart1.render();

 }
</script>
    <div class="page-wrapper">
        <div class="content container-fluid">

            <!-- Page Header -->
            <div class="page-header">
                <div class="row">
                    <div class="col-12">
                        <h3 class="page-title">Welcome Admin!</h3>
                    </div>
                </div>
            </div>
            <!-- /Page Header -->

            <div class="row">
                <div class="col-xl-3 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
									<span class="dash-widget-icon active">
										<i class="far fa-user"></i>
									</span>
                                    @foreach ($Revenue as $revenue)

                                    @endforeach
                                <div class="dash-widget-info">
                                    <h3>R{{ number_format( sprintf( '%.2f', $revenue->totalSales), 2, '.', '' ) }}</h3>
                                    <h6 class="text-muted">Total Sales</h6>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
									<span class="dash-widget-icon active">
										<i class="fas fa-user-shield"></i>
									</span>
                                    @foreach ($TotalOrders as $totalOrders)

                                    @endforeach
                                <div class="dash-widget-info">
                                    <h3>{{ $totalOrders->totalOrders }}</h3>
                                    <h6 class="text-muted">Total Orders</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
									<span class="dash-widget-icon active">
										<i class="fas fa-qrcode"></i>
									</span>
                                    @foreach ($TotalSalesByCurrentMonth as $totalSalesByCurrentMonth)

                                    @endforeach
                                <div class="dash-widget-info">
                                    <h3>R{{ number_format( sprintf( '%.2f', $totalSalesByCurrentMonth->totalSales), 2, '.', '' ) }}</h3>
                                    <h6 class="text-muted">Monthly Revenue</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
									<span class="dash-widget-icon active">
										<i class="far fa-credit-card"></i>
									</span>
                                    @foreach ($TotalOrdersByCurrentMonth as $totalOrdersByCurrentMonth)

                                    @endforeach
                                <div class="dash-widget-info">
                                    <h3>{{ $totalOrdersByCurrentMonth->totalOrders }}</h3>
                                    <h6 class="text-muted">Monthly Orders</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 d-flex">

                    <!-- Recent Bookings -->
                    <div class="card card-table flex-fill">
                        <div class="card-header">
                            <h4 class="card-title">Total Sales By Month</h4>
                        </div>
                        <div class="card-body">
                            <!-- <br>
                          <input type="month"> -->
                           <div id="chartContainer" style="height: 370px; width: 450px;"></div>
                        </div>
                    </div>
                    <!-- /Recent Bookings -->

                </div>
                <div class="col-md-6 d-flex">

                    <!-- Payments -->
                    <div class="card card-table flex-fill">
                        <div class="card-header">
                            <h4 class="card-title">Total No. of Orders By Month</h4>
                        </div>
                        <div class="card-body">
                            <div id="chartContainer1" style="height: 370px; width: 450px;"></div>
                        </div>
                    </div>
                    <!-- Payments -->

                </div>
            </div>
            <div class="row">
                <div class="col-md-12 d-flex">

                    <!-- Recent Bookings -->
                    <div class="card card-table flex-fill">
                        <div class="card-header">
                            <h4 class="card-title">Recent Orders</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-center">
                                    <thead>
                                    <tr>
                                        <th>Product Image</th>
                                        <th>Firstname</th>
                                        <th>Lastname</th>
                                        <th>Order Date</th>
                                        <th>Order Status</th>
                                        <th>Amount</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($recentCustOrders as $RecentCustOrders)
                                    <tr>
                                        <td class="text-nowrap">
                                            <img class="avatar-xs rounded-circle" src="/admin/img/customer/user-05.jpg" alt="User Image"> Annette Silva
                                        </td>
                                        <td class="text-nowrap">{{$RecentCustOrders->Firstname}}</td>
                                        <td>{{$RecentCustOrders->Lastname}}</td>
                                        <td>{{$RecentCustOrders->OrderDate}}</td>
                                        <td> <span class="badge bg-danger inv-badge" id="statusVal">{{$RecentCustOrders->OrderStatus}}</span></td>
                                        <td>{{$RecentCustOrders->Amount}}</td>
                                        <td class="text-right">
                                            <a href="#" class= " btn btn-sm bg-success-light mr-2">	<i class="far fa-edit mr-1"></i> Edit</a>
                                            <a href="#" class= " btn btn-sm bg-danger-light mr-2">	<i class="fa fa-trash"></i> Delete</a>
                                        </td>
                                    </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /Recent Bookings -->

                </div>

            </div>
        </div>
    </div>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
@endsection
