<?php

use App\Models\catalogue;
use App\Models\categories;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomAuthController;
use App\Http\Controllers\CatalogueController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\WishlistController;
use App\Http\Controllers\ProductGridController;
use App\Http\Controllers\ProductDetailsController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\WelcomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    Route::get('/', function () {
    $allProducts= Catalogue::all();

    $categories= Categories::all();
    //dd($allProducts);
//    $girlsProducts= Catalogue::all()->where('ProductCode',"G001");
//    $boysProducts= Catalogue::all()->where('ProductCode',"B001");

    $productsOnSale= Catalogue::all()->where('ProductPromotionStatus',"Sale" );
    $ProductId = '2021-11-29 15:34:52';
    $returnedResult = DB::table('Catalogues')->whereRaw('ProductId = ?',[$ProductId])->selectRaw('*')->get();
    $newProducts= Catalogue::all()->where('ProductPromotionStatus',"New Arrival");

    return view('welcome', compact('productsOnSale','returnedResult','newProducts','allProducts'));
        // ->with('productsOnSale',$productsOnSale)
        // ->with('returnedResult',$returnedResult)
        // ->with('newProducts',$newProducts)
        // ->with('allProducts',$allProducts);
});

Route::get('welcome', [WelcomeController::class, 'index'])->name('welcome');
// Auth Routes
Route::get('dashboard', [CustomAuthController::class, 'dashboard'])->name('dashboard');
Route::get('login', [CustomAuthController::class, 'index'])->name('login');
Route::post('custom-login', [CustomAuthController::class, 'customLogin'])->name('login.custom');
Route::get('registration', [CustomAuthController::class, 'registration'])->name('register');
Route::post('custom-registration', [CustomAuthController::class, 'customRegistration'])->name('register.custom');
Route::get('signout', [CustomAuthController::class, 'signOut'])->name('signout');

//Admin Backend Routes
Route::get('/catalogue',[CatalogueController::class, 'index']);
Route::get('/categories',[CategoriesController::class, 'index']);
Route::get('/subCategories',[SubCategoryController::class, 'index']);


// Product Catalogue Routes
Route::post('/addCatalogue',[App\Http\Controllers\CatalogueController::class, 'store'])->name('catalogue.store');
Route::post('/editCatalogue/{id}',[CatalogueController::class, 'update']);
Route::get('/catalogue/delete/{id}',[CatalogueController::class, 'destroy'])->name('catalogueDelete');
Route::post('/importCatalogue', [CatalogueController::class, 'import']);//->name('import_Catalogue');

// product Category routes
Route::post('addCategory',[CategoriesController::class, 'store'])->name('categories.store');
Route::get('admin/categories/delete/{id}',[CategoriesController::class, 'destroy']);
Route::get('admin/categories/edit/{id}',[CategoriesController::class, 'edit'])->name('categoryDelete');
Route::post('/importCategory', [CategoriesController::class, 'import'])->name('import_Category');

//frontend routes -cart

// Route::get('productDetails/{id}', [ProductDetailsController::class, 'productDetails']);
// Route::get('addToCart', [CartController::class, 'addToCart']);
// Route::get('cart', [CartController::class,'cart']);


Route::get('/updateCart', [CartController::class, 'update']);
Route::get('/removeFromCart/{id}', [CartController::class, 'remove'])->name('removeFromCart');
Route::get('/checkout', [CheckoutController::class, 'index'])->name('checkout');
Route::get('viewCart', [CartController::class, 'viewCart'])->name('viewCart');



//Checkout routes
Route::get('/notify', [CheckoutController::class, 'index']);
Route::get('/cancel', [ViewCartController::class, 'index']);
Route::get('/success', [CheckoutController::class, 'index']);

//Wishlist Routes
Route::get('/wishlist', [WishlistController::class, 'index'])->name('wishlist');
Route::post('/wishlistProducts/{id}', [WishlistController::class, 'addToWishlist'])->name('wishlistProducts');
Route::get('/addToCart/{id}', [CartController::class, 'addToCart']);

//Batch Import
// Route::post('/importCatalogue', [CatalogueController::class, 'import'])-name('importCatalogue');


//shop
Route::get('/shop',[ProductGridController::class, 'index'])->name('shop');
Route::get('viewByCategory/{category}',[ProductGridController::class, 'viewByCategory'])->name('viewByCategory');
Route::get('productDetails/{ProductId}',[ProductDetailsController::class, 'index'])->name('productDetails');
//Route::get('addToCart/{id}', [CartController::class, 'addToCart']);
Route::post('addToCart',[CartController::class, 'addToCart'])->name('cart.addToCart');
Route::post('cart/{id}', [CartController::class, 'cart']);
Route::patch('update-cart', [CartController::class, 'update'])->name('update.cart');
Route::delete('remove-from-cart', [CartController::class, 'remove'])->name('remove.from.cart');
Route::get('/checkout', [CheckoutController::class, 'index']);
