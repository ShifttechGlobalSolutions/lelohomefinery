<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->increments('OrderDetailsId');
            $table->integer('OrderId');
            $table->string('ProductId');
            $table->double('ProductPrice');
            $table->integer('ProductQty');
            $table->timestamps();

            // $table->foreign('OrderId')->references('OrderId')->on('orders')->onDelete('cascade')->onupdate('cascade');
            // $table->foreign('productId')->references('productId')->on('catalogue')->onDelete('cascade')->onupdate('cascade');;
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
