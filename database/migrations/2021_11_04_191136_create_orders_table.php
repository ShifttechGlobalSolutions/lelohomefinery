<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('OrderId');
            $table->integer('CustomerId');
            $table->double('Amount');
            $table->datetime('OrderDate');
            $table->datetime('RequiredDate');
            $table->string('OrderStatus');
            $table->timestamps();

            // $table->foreign('CustomerId')->references('CustomerId')->on('customers')->onDelete('cascade')->onupdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
