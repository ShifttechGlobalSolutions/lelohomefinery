<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('customerId');
            $table->string('Firstname');
            $table->string('Lastname');
            $table->string('CustomerEmailAddress');
            $table->string('Phone');
            $table->text('StreetAddress');
            $table->string('Appartment_Suite_Unit');
            $table->string('City');
            $table->string('Province');
            $table->string('Country');
            $table->string('PostalCode');
            $table->string('Password');
            $table->text('Notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
